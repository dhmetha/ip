FROM nginx:stable-alpine-slim

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

COPY ./favicon.ico /usr/share/nginx/html/favicon.ico
COPY ./index.html /usr/share/nginx/html/index.html
